﻿using System;

namespace Training.Library
{
    public class Shape
    {
        public virtual decimal getArea()
        {
            Console.WriteLine("Shape area");
            return 1;
        }


    }

    public class Square : Shape
    {
        private decimal length;
     
        public Square(decimal len)
        {
            this.length = len;
        }

        public override decimal getArea()
        {
            // return base.getArea();
            return this.length * this.length;
        }

    }

    public class Circle : Shape
    {
        private decimal radius;

        public Circle(decimal rad)
        {
            this.radius = rad;
        }
        public override decimal getArea()
        {
            //return base.getArea();
            decimal area = Convert.ToDecimal(2 * 3.14) * this.radius * this.radius;
            return area;
        }

    }
}

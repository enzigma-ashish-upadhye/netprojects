﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Training.Library;
namespace TestShape
{
    [TestClass]
    public class UnitTest1
    {
        
        [TestMethod]
        public void TestMethodShape()
        {
            var shape = new Shape();
            var result = shape.getArea();
            Assert.IsNotNull(result);
        }
        
        [TestMethod]
        public void TestMethodSquare()
        {
            var square = new Square(10);
            var result = square.getArea();
            Assert.AreEqual(100, result);
        }

        [TestMethod]
        public void TestMethodCircle()
        {
            var circle = new Circle(10);
            var result = circle.getArea();
            Assert.AreEqual(628, result);
        }
    }



}

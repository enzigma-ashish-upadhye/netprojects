﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace LinqAndLambdas
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Person> people = new List<Person>()
            {
                new Person("Ashish", 160, 55),
                new Person("Amit", 150, 57),
                new Person("Mukesh", 155, 65)

            };

            var fourCharPeople = from p in people
                               //  where (p.Name.Length == 4)
                                 orderby p.Height
                                 select p;

            foreach (var item in fourCharPeople)
            {
                Console.WriteLine($"Name :{item.Name} weight : {item.Weight}");
                Console.ReadLine();
            
            }
        }
    }
}

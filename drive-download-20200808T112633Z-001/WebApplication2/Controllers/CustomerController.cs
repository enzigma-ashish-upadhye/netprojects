﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BOL;
using BLL;
namespace WebApplication2.Controllers
{
    public class CustomerController : Controller
    {
        // GET: Customer
        public ActionResult Index()
        {
            List<Customer> customer = CustomerManager.GetAllCust();
            return View(customer);
          
        }

        // GET: Customer/Details/5
        public ActionResult Details(int id)
        {
            Customer cust = CustomerManager.GetCustomerById(id);
            return View(cust);
        }
      //Login Method Calling 
            [HttpGet]
            public ActionResult Login()
            {
                return View();
            }

        [HttpPost]
        public ActionResult Login(string email, string fname)
        {

            if (CustomerManager.AuthenticateUser(email, fname))
            {
                return RedirectToAction("About", "Home");
            }
            else
            {
                return View();
            }
        }

        
        public ActionResult MainPage()
        {
            return View();
        }









        //End of login method

        // GET: Customer/Create
        [HttpGet]
        public ActionResult Insert()
        {
            return View();
        }

        // POST: Customer/Create
        [HttpPost]
        public ActionResult Insert(int id,string fName,string lName,string email,int age)
        {
            Customer customer = new Customer { Id = id, FirstName = fName, LastName = lName, Email = email, Age = age };
           bool status= CustomerManager.RegisterUser(customer);
            return View();
        }

        // GET: Customer/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Customer/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Customer/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Customer/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}

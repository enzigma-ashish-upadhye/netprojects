﻿using BOL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
namespace BLL
{
    public class CustomerManager
    {
        public static List<Customer> GetAllCust()
        {
            List<Customer> custs = new List<Customer>();
            custs = DBCustomerManager.GetAllCustomers();
            return custs;
        }
        public static Customer GetCustomerById(int id)
        {
            Customer customer= null;
            customer = DBCustomerManager.GetCustomerById(id);
            return customer;
        }
        public static bool RegisterUser(Customer cust)
        {
            bool status = false;

            bool res = DBCustomerManager.RegisterCustomer(cust);
            if(res == true)
            {
                status = true;
            }
            return status;
        }

        public static bool AuthenticateUser(string email,string fname)
        {
            bool status = false;
            status = DBCustomerManager.GetAuthenticateUser(email, fname);
            return status;
        }
    }
}

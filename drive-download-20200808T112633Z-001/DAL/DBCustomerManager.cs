﻿using BOL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class DBCustomerManager
    {
        public static string SqlCon = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=E:\Ashish\.Net\drive-download-20200808T112633Z-001\WebApplication2\App_Data\Database.mdf;Integrated Security=True";
        public static List<Customer> GetAllCustomers()
        {
             List<Customer> customers= new  List<Customer>();

            string sqlString ="select * from customer";

            IDbConnection con = new SqlConnection();
            con.ConnectionString = SqlCon;

            IDbCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandText = sqlString;


            IDataReader reader = null;

            try
            {
                con.Open();
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    Customer customer = new Customer();
                    customer.Id = int.Parse(reader["Id"].ToString());
                    customer.FirstName = reader["FirstName"].ToString();
                    customer.LastName = reader["LastName"].ToString();
                    customer.Email = reader["Email"].ToString();
                    customer.Age = int.Parse(reader["Age"].ToString());

                    customers.Add(customer);
                }
            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                con.Close();
            }
            return customers;
        }
        public static Customer GetCustomerById(int id)
        {
            Customer customer = null;
           string cmdString = "select * from customer where Id=@id";
            
            return customer;
        }
        
        public static Boolean RegisterCustomer(Customer cust)
        {
            string cmdString = "insert into customer values(@Id,@FirstName,@LastName,@Email,@Age)";

            Boolean status = false;
            IDbConnection con = new SqlConnection();
            con.ConnectionString = SqlCon;
            IDbCommand cmd = new SqlCommand();
                                 

            cmd.Connection = con;
            cmd.CommandText = cmdString;
            
            cmd.Parameters.Add(new SqlParameter("@Id", cust.Id));
            cmd.Parameters.Add(new SqlParameter("@Firstname", cust.FirstName));
            cmd.Parameters.Add(new SqlParameter("@LastName", cust.LastName));
            cmd.Parameters.Add(new SqlParameter("@Email", cust.Email));
            cmd.Parameters.Add(new SqlParameter("@Age", cust.Age));

            try
            {
                con.Open();
                int res = cmd.ExecuteNonQuery();
                if (res > 0)
                {
                    status = true;
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return status;
        } 
        
        public static bool GetAuthenticateUser(string email,string fname)
        {
            bool status = false;
            string cmdString = "select * from customer where Email=@email and FirstName=@fname";
            IDbConnection con =new SqlConnection();
            con.ConnectionString = SqlCon;
            IDbCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandText = cmdString;
            cmd.Parameters.Add(new SqlParameter ( "@email", email ));
            cmd.Parameters.Add(new SqlParameter("@fname", fname));
            IDataReader reader =null;

            try
            {
                con.Open();
                reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    status = true;
                }
            }catch(Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return status;
        }
    }
}
